import { RouteConfig } from 'vue-router';
import { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ElLoadingComponent } from 'element-ui/types/loading';

declare module 'vue/types/vue' {

    interface Vue {
        $validator: (formRef: string) => Promise<any>;
        $clearValidator: (formRef: string) => void;
        $http: AxiosInstance;
        $setTitle: (str: string) => void;
        $keLoading: (message: string) => ElLoadingComponent;
        $array: {
            update: (list: any[], key: any, data: any, pk?: string) => void;
            delete: (list: any[], key: any, pk?: string) => void;
            tree: (list: any[], id?: string, pid?: string, children?: string) => any[];
        };
    }
}

declare module 'vue-router/types/router' {
    interface _RouteConfigBase {
        sort?: number;
        module?: string | undefined;
        hidden?: boolean;
        opened?: boolean;
    }
}

declare module 'axios' {
    export interface AxiosRequestConfig {
        message?: boolean;
    }
}
