import { Component, Mixins } from 'vue-property-decorator';
import AdminContainer from '@/components/admin-container.vue';
import { KBuilderForm } from '@/components/k-builder/form';
import { ConfigOption } from '@/components/k-builder/types/form';


@Component({
    components: { AdminContainer }
})
export default class Page extends Mixins(KBuilderForm) {

    build() {

        this.setConfig({
            labelWidth: '120px',
            closeValidate: true,
        });

        this.addInput({
            label: '名称',
            prop: 'name',
            required: true,
        });

        this.addInput({
            label: '标签',
            prop: 'title',
            required: true,
        });

        this.addCheckbox({
            label: '标签',
            prop: 'checkbox',
            required: true,
            optionLoad: () => {
                return new Promise<ConfigOption[]>(resolve => {
                    setTimeout(() => {
                        resolve([
                            { label: 'a', value: 1 },
                            { label: 'b', value: 2 },
                            { label: 'c', value: 3 },
                            { label: 'd', value: 4 },
                        ])
                    }, 3000)
                });
            }
        });

        this.addRadio({
            label: '标签',
            prop: 'radio',
            required: true,
            defaultValue: 3,
            options: [
                { label: 'a', value: 1 },
                { label: 'b', value: 2 },
                { label: 'c', value: 3 },
                { label: 'd', value: 4 },
            ]
        });

        this.addSelect({
            label: '标签',
            prop: 'select',
            required: true,
            defaultValue: 3,
            options: [
                { label: 'a', value: 1 },
                { label: 'b', value: 2 },
                { label: 'c', value: 3 },
                { label: 'd', value: 4 },
            ]
        });

        this.addDate({
            label: 'date',
            prop: 'date',
            required: true,
            format: 'yyyy年M月'
        });

        this.addImage({
            label: '图片',
            prop: 'image'
        });

        this.addTextarea({
            label: '文本域',
            prop: 'textarea',
            rows: 6
        });

        this.addMarkdown({
            label: 'markdown',
            prop: 'markdown',
        });

        this.onSubmit((data: any) => {
            return new Promise<any>(resolve => {
                console.log('submit', data)
                resolve()
            });
        });
    }
}
