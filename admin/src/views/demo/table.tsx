import { Component, Mixins } from 'vue-property-decorator';
import AdminContainer from '@/components/admin-container.vue';
import { KBuilderTable } from '@/components/k-builder/table';


@Component({
    components: { AdminContainer }
})
export default class Page extends Mixins(KBuilderTable) {

    protected build() {
        this.setConfig({
            url: {
                data: 'articles',
                edit: false,
                delete: false,
            },
            btns: [
                {
                    label: '导出Excel',
                    onClick: () => {
                        this.$message.success('导出Excel')
                    }
                }
            ]
        });
        this.addColumn({ label: 'ID', prop: 'id' })
            .addColumn({ label: '标题', prop: 'title' })
            .addColumn({ label: '浏览量', prop: 'view_count' })
            .addColumn({ label: '发布时间', prop: 'create_time' })
    }

}
