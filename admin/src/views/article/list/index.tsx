import { Component } from 'vue-property-decorator';
import AdminContainer from '@/components/admin-container.vue';
import { KBuilderTable } from '@/components/k-builder/table';

@Component({
    components: {
        AdminContainer
    }
})
export default class Article extends KBuilderTable {

    protected build() {
        this.setConfig({
            url: {
                data: 'articles',
                add: 'article/post',
                edit: 'article/post',
                delete: 'article'
            }
        })
        this.addColumn({ label: 'ID', prop: 'id' })
            .addColumn({ label: '标题', prop: 'title' })
            .addColumn({ label: '关键词', prop: 'keyword' })
            .addColumn({ label: '创建时间', prop: 'create_time' })
            .addColumn({ label: '更新时间', prop: 'update_time' })
    }

}
