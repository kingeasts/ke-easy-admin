export interface ArticleCategoryModel {
    id: number;
    pid: number;
    path: string;
    name: string;
    sort: number;
}
