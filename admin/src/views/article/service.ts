import http from '@/plugins/http';
import { ArticleCategoryModel } from '@/views/article/model';

/**
 * 文章列表
 * @param query
 */
export const getArticles = (query?: any) => {
    return http.get('admin/articles', {
        params: query,
    });
}

/**
 * 文章详情
 * @param id
 */
export const getArticle = (id: number) => {
    return http.get(`admin/article/${id}`);
}

/**
 * 新建 / 编辑文章
 * @param isEdit
 * @param data
 */
export const postArticle = (isEdit: boolean, data: any) => {
    return http.post(`admin/article/${isEdit ? 'edit': 'add'}`, data);
}

/**
 * 删除文章
 * @param id
 */
export const deleteArticle = (id: number[]) => {
    return http.delete(`admin/article/${id.join(',')}`);
}

/**
 * 获取文章分类
 */
export const getArticleCates = async (): Promise<ArticleCategoryModel[]> => {
    const res = await http.get('admin/article/categories')

    return res.data.items;
}

/**
 * 新建 / 编辑文章分类
 * @param isEdit
 * @param data
 */
export const postArticleCate = (isEdit: boolean, data: any) => {
    return http.post(`admin/article/category/${isEdit ? 'edit': 'add'}`, data);
}

/**
 * 删除文章分类
 * @param id
 */
export const deleteArticleCate = async (id: number[]): Promise<void> => {
    await http.delete('admin/article/category', {
        data: { id }
    });
}
