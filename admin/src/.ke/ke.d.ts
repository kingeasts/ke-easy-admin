export namespace KE {
    export interface Settings {
        topNavs: TopNav[];
    }

    export interface TopNav {
        name: string;
        text: string;
        url?: string;
    }
}
