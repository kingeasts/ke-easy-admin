import Vue from 'vue';
import { AuthModule } from '@/store/auth';

function checkPermission(el: any, binding: any) {
    const { value }: { value: string } = binding
    if (value && !AuthModule.policys.includes(value)) {
        el.parentNode && el.parentNode.removeChild(el)
    }
}

Vue.directive('permission', {
    inserted: checkPermission,
    update: checkPermission,
})
