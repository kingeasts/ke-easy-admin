import Vue from 'vue';


Vue.directive('submit', {
    inserted(el, node) {
        el.onsubmit = (evt) => {
            evt.preventDefault();
            node.value && node.value();
        }
    },
    update(el, node) {
        el.onsubmit = (evt) => {
            evt.preventDefault();
            node.value && node.value();
        }
    }
})
