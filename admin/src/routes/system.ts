import Layout from '@/layout/index/index.vue';
import { RouteConfig } from 'vue-router';

const route = {
    path: '/system',
    module: 'system',
    component: Layout,
};

export default <RouteConfig[]>[
    {
        path: '/system',
        module: 'system',
        redirect: '/system/setting',
        sort: 100,
        component: Layout,
        children: [
            {
                path: 'setting',
                name: 'System',
                sort: 200,
                module: 'system',
                meta: {
                    title: '系统',
                    auth: 'Setting'
                },
                component: () => import('@/views/system/setting/index.vue')
            },
        ]
    },
    {
        path: '/acl',
        module: 'system',
        sort: 100,
        opened: true,
        meta: {
            title: '权限管理',
            auth: 'Acl'
        },
        component: Layout,
        children: [
            // 管理员
            {
                path: 'users',
                name: 'AclUsers',
                meta: {
                    title: '管理员'
                },
                component: () => import('@/views/system/acl/users/index.vue'),
                children: [
                    {
                        path: 'edit',
                        name: 'AclUsersEdit',
                        meta: {
                            active: 'AclUsers'
                        },
                        component: () => import('@/views/system/acl/users/form.vue'),
                    }
                ]
            },

            // 管理员
            {
                path: 'department',
                name: 'AclDepartments',
                meta: {
                    title: '组织管理'
                },
                component: () => import('@/views/system/acl/department/index.vue')
            },

            // 角色管理
            {
                path: 'role',
                name: 'AclRole',
                meta: {
                    title: '角色管理'
                },
                component: () => import('@/views/system/acl/role/index.vue'),
                children: [
                    {
                        path: 'edit',
                        name: 'AclRoleEdit',
                        meta: {
                            active: 'AclRole',
                            title: '角色管理'
                        },
                        component: () => import('@/views/system/acl/role/form.vue'),
                    }
                ]
            },
            {
                path: 'role/rule',
                name: 'AclRoleAuth',
                hidden: true,
                meta: {
                    title: '权限管理',
                    active: 'AclRole',
                },
                component: () => import('@/views/system/acl/role/auth.vue')
            },
        ]
    },
]
