import { RouteConfig } from 'vue-router';
import Layout from '../layout/index/index.vue';

/**
 * module 模块分组
 * sort 在侧边栏的排序,从小到大排序
 * opened 如果有子菜单时是否默认展开
 * meta.title 标题
 * meta.noCache 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
 * meta.auth 使用权限，留空则不判定权限
 * meta.top 是否显示在顶部菜单,只针对一级路由有效
 */
export default <RouteConfig[]>[
    {
        path: '/demo',
        sort: 200,
        module: 'index',
        redirect: '/demo/table',
        opened: true,
        meta: {
            title: 'Demo'
        },
        component: Layout,
        children: [
            {
                path: 'table',
                name: 'DemoTable',
                sort: 200,
                module: 'index',
                meta: {
                    title: '表格构建类'
                },
                component: () => import('@/views/demo/table')
            },
            {
                path: 'form',
                name: 'DemoForm',
                sort: 200,
                module: 'index',
                meta: {
                    title: '表单构建类'
                },
                component: () => import('@/views/demo/form')
            }
        ]
    },
    {
        path: '/user',
        name: 'UserMs',
        sort: 200,
        module: 'index',
        redirect: '/user/index',
        meta: {
            title: '用户管理'
        },
        component: Layout,
        children: [
            {
                path: 'index',
                name: 'User',
                sort: 200,
                module: 'index',
                meta: {
                    title: '用户列表',
                    auth: 'User'
                },
                component: () => import('@/views/user/index.vue')
            },
            {
                path: 'post/:id',
                name: 'UserEdit',
                hidden: true,
                meta: {
                    active: 'User',
                    noCache: true,
                    auth: 'User'
                },
                component: () => import('@/views/user/post.vue')
            },
            {
                path: 'recharge',
                name: 'UserRecharge',
                meta: {
                    title: '用户充值',
                    noCache: true,
                    auth: 'UserRecharge'
                },
                component: () => import('@/views/user/recharge.vue')
            },
            {
                path: 'logs',
                name: 'UserLogs',
                sort: 300,
                meta: {
                    title: '财务日志',
                    auth: 'UserFinance'
                },
                component: () => import('@/views/user/logs.vue')
            }
        ]
    },
    {
        path: '/article',
        sort: 300,
        module: 'index',
        redirect: '/article/index',
        meta: {
            title: '文章管理'
        },
        component: Layout,
        children: [
            {
                path: 'category',
                name: 'ArticleCategory',
                sort: 400,
                meta: {
                    title: '分类管理',
                    auth: 'ArticleCategory'
                },
                component: () => import('@/views/article/category/index.vue')
            },
            {
                path: 'index',
                name: 'Article',
                sort: 200,
                module: 'index',
                meta: {
                    title: '文章列表',
                    auth: 'Article'
                },
                component: () => import('@/views/article/list/index')
            },
            {
                path: 'post',
                name: 'ArticleEdit',
                hidden: true,
                meta: {
                    active: 'Article',
                    noCache: true,
                    auth: 'Article'
                },
                component: () => import('@/views/article/list/form.vue')
            }
        ]
    }
]
