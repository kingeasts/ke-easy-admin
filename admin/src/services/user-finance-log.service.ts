import http from '@/plugins/http';
import { AxiosResponse } from 'axios';

// 获取财务列表
export const getUserFinanceLogList = async (query?: any): Promise<AxiosResponse> => {
    return await http.get('admin/user/finance_logs', { params: query });
}
