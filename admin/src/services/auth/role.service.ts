import { AxiosResponse } from 'axios';
import http from '@/plugins/http';
import { AdminRoleModel } from '@/models/admin-role.model';

// 获取角色列表
export const getAuthRoles = (query?: any) => {
    return http.get('admin/roles', { params: query });
}

// 创建 / 编辑角色
export const postAuthRole = (data: AdminRoleModel) => {
    return http.post('admin/role', data);
}

/**
 * 保存策略
 * @param id
 * @param data
 */
export const setRolePolicy = (id: number, data: string[]) => {
    return http.post(`admin/role/${id}/set_policy`, { policys: data });
}


/**
 * 角色拥有的策略
 * @param id
 */
export const getRolePolicy = (id: number) => {
    return http.get(`admin/role/${id}/policys`);
}
