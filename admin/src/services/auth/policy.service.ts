import http from '@/plugins/http';

/**
 * 获取策略列表
 */
export const getPolicys = () => {
    return http.get('admin/policys');
}
