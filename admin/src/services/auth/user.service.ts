import { AxiosResponse } from 'axios';
import http from '@/plugins/http';

// 获取管理员列表
export const getSuperUsers = async (query?: any) => {
    return await http.get(`admin/super/users`, {
        params: query,
    });
}

// 获取管理员
export const getSuperUser = async (id: number) => {
    return await http.get(`admin/super/user/${id}`);
}

/**
 * 添加 / 编辑管理
 * @param is_edit
 * @param data
 */
export const saveSuperUser = (is_edit: boolean, data: any) => {
    return http.post('admin/super/user/' + (is_edit ? 'edit' : 'add'), data);
}

// 删除管理
export const deleteSuperUser = async (id: number): Promise<AxiosResponse> => {
    return await http.delete(`admin/super/user/${id}`)
}
