import http from '@/plugins/http';
import { AdminDepartmentModel } from '@/models/admin-department.model';

/**
 * 组织列表
 * @param query
 */
export const getDepartments = (query?: any) => {
    return http.get('admin/super/departments', { params: query });
}

/**
 * 添加 / 编辑组织
 * @param action
 * @param data
 */
export const postDepartment = (action: string, data: AdminDepartmentModel) => {
    return http.post('admin/super/department/' + action, data);
}

/**
 * 删除组织
 * @param id
 */
export const deleteDepartment = (id: number) => {
    return http.delete(`admin/super/department/${id}`);
}
