import http from '@/plugins/http';
import { AuthEditInfoModel } from '@/models/auth.model';
import { AccountLoginModel } from '@/models/account.login.model';


export interface CurrentData {
    info: CurrentUser;
    policys: string[];
}

export interface CurrentUser {
    id: number;
    username: string;
    nickname: string;
    avatar: string;
    phone: string;
    status: number;
    create_time: string;
    update_time: string;
    login_ip: number;
    login_time: number;
    login_count: number;
    login_fail_time: number;
    login_fail_count: number;
    permission: any[];
    department_id: number;
}


/**
 * 获取当前页用户信息
 */
export const getCurrentAccount = (): Promise<CurrentData> => {
    return http.get('admin/account/current').then(result => result.data);
}

/**
 * 修改登录密码
 * @param data
 */
export const changeAccountPassword = (data: any) => {
    return http.post('admin/account/change_password', data);
}

/**
 * 修改个人信息
 * @param data
 */
export const editAccountInfo = (data: AuthEditInfoModel) => {
    return http.post('admin/account/edit_info', data);
}

/**
 * 登陆
 * @param data
 * @constructor
 */
export const AccountLogin = (data: AccountLoginModel): Promise<{
    data: {
        access_token: string,
        expire_in: number,
        info: any,
        policys: string[],
    }
}> => {
    return http.post('admin/account/login', data);
}
