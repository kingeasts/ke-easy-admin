import http from '@/plugins/http';

export const getAdminActionLogs = (query?: any) => {
    return http.get('admin/action/logs', {
        params: query,
    });
}
