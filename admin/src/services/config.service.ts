import http from '@/plugins/http';

export const getConfigList = () => {
    return http.get('admin/config/nodes');
}

export const postConfig = (data: {[key: number]: any}) => {
    return http.post('admin/config', { data });
}
