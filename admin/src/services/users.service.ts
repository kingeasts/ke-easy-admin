import http from '@/plugins/http';
import { AxiosResponse } from 'axios';
import { UserModel } from '@/models/user.model';

// 用户列表
export const getUserList = async (query?: any): Promise<AxiosResponse> => {
    return await http.get('admin/users', { params: query });
}

// 获取用户
export const getUser = async (id: number): Promise<AxiosResponse> => {
    return await http.get(`admin/user/${id}`);
}

// 修改用户信息
export const editUser = async (id: number, data: UserModel): Promise<AxiosResponse> => {
    return await http.post(`admin/user/${id}`, data);
}

// 删除用户
export const deleteUser = async (id: number): Promise<AxiosResponse> => {
    return await http.delete(`admin/user/${id}`)
}
