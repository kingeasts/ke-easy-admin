import http from '@/plugins/http';

// 文件分组
export const getFileGroups = () => {
    return http.get('admin/file/groups');
};

// 获取相册图片列表
export const getUploadImages = (page: number, limit: number, group_id: number) => {
    return http.get('admin/upload/images', {
        params: {
            page,
            limit,
            group_id,
        }
    });
};

// 上传图片
export const uploadImage = (options: {
    group_id: number,
    file: File,
    onUploadProgress?: (progress: any) => void,
}) => {
    let fd = new FormData();
    fd.append('group_id', options.group_id.toString());
    fd.append('file', options.file);
    return http.post('admin/upload/image', fd, {
        headers: {},
        message: false,
        onUploadProgress: options?.onUploadProgress,
    });
};
