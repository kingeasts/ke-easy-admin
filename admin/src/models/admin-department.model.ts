export interface AdminDepartmentModel {
    id: number;
    parent_id: number;
    name: string;
    children?: AdminDepartmentModel[];
}
