export interface AdminRoleModel {
    readonly id: number;
    name: string;
    remark: string;
    sort: number;
}
