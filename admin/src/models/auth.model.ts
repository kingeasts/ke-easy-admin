export interface AuthModel {
    id: number;
    username: string;
    nickname: string;
    avatar: string;
    phone: string;
    status: number;
    create_time: number;
    update_time: number;
    login_ip: string;
    login_time: number;
    login_count: number;
}

// 编辑个人信息模型
export interface AuthEditInfoModel {
    nickname: string;
    avatar: string;
    phone: string;
}
