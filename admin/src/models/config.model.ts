export interface ConfigModel {
    id: number;
    key: string;
    title: string;
    value: any;
    grouping: string;
    description: string;
    component: string;
    data_type: string;
    visible_condition: string;
    sort: number;
    options: ConfigOptionsModel[] | null;
    extends: object | null;
    children: ConfigModel[] | undefined;
}

export interface ConfigOptionsModel {
    text: string;
    value: any;
}
