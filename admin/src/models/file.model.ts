export interface FileModel {
    id: number;
    group_id: number;
    type: string;
    filename: string;
    mime: string;
    src: string;
    create_time: string;
}
