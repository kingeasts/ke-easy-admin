export interface UserFinanceLogModel {
    id: number;
    user_id: number;
    type: 'money' | 'score';
    value: number;
    remark: string;
    create_time: string;
}
