export interface ActionActiveModel {
    id: number;
    user: {
        avatar: string,
        nickname: string
    };
    message: string;
    create_time: string;
}
