export interface UserModel {
    id: number;
    username: string;
    password: string;
    nickname: string;
    avatar: string;
    sex: 0 | 1;
    age: number;
    email: string;
    mobile: string;
    status: number;
    money: number;
    score: number;
    create_time: string;
    update_time: string;
}
