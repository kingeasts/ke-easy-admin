import Vue from 'vue';

const title = document.title;

Vue.prototype.$setTitle = (str: string) => {
    document.title = `${str} - ${title}`;
}

export const setTitle = Vue.prototype.$setTitle
