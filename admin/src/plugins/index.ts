import Vue from 'vue';
import Utils from './utils';
import './element';
import './http';
import './meta';

Vue.use(Utils);
