import Vue from 'vue';
import VueScroll from 'vuescroll';
import {
    Message,
    MessageBox,
    Notification,
    Loading,
    Button,
    ButtonGroup,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    Input,
    Select,
    Option,
    Radio,
    RadioGroup,
    Checkbox,
    CheckboxGroup,
    Form,
    FormItem,
    DatePicker,
    Drawer,
    Dialog,
    Row,
    Col,
    Card,
    Table,
    TableColumn,
    Breadcrumb,
    BreadcrumbItem,
    Pagination,
    Tabs,
    TabPane,
    Transfer,
    Tree,
    Alert,
    PageHeader,
    Image,
    Progress,
    Divider,
} from 'element-ui';
import ElementUi from 'element-ui';

Vue.use(ElementUi);

Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };

// @ts-ignore
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';

Vue.component(VueScroll.name, VueScroll)

Vue.prototype.$message = Message
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$loading = Loading.service
Vue.prototype.$keLoading = (message: string) => {
    return Loading.service({
        text: message,
        target: '#admin-container'
    })
}

export const $message = Message
export const $alert = MessageBox.alert
export const $confirm = MessageBox.confirm
export const $loading = Loading.service

export const $notify = Notification;

Vue.use(Loading)

Vue.use(Button)
    .use(ButtonGroup)
    .use(Dropdown)
    .use(DropdownItem)
    .use(DropdownMenu)
    .use(Input)
    .use(Select)
    .use(Option)
    .use(Radio)
    .use(RadioGroup)
    .use(Checkbox)
    .use(CheckboxGroup)
    .use(Form)
    .use(FormItem)
    .use(DatePicker)
    .use(Drawer)
    .use(Dialog)
    .use(Row)
    .use(Col)
    .use(Card)
    .use(Table)
    .use(TableColumn)
    .use(Breadcrumb)
    .use(BreadcrumbItem)
    .use(Pagination)
    .use(Tabs)
    .use(TabPane)
    .use(Transfer)
    .use(Tree)
    .use(Alert)
    .use(PageHeader)
    .use(Image)
    .use(Progress)
    .use(Divider)

Vue.component(CollapseTransition.name, CollapseTransition)
