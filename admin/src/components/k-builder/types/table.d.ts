/**
 * 表格配置
 */
export interface KBuilderConfig {
    // url配置
    url?: {
        // 请求接口前缀
        prefix?: string;
        // 列表数据获取，默认：GET 当前路由s
        data?: string;
        // 执行删除接口，默认：DELETE 当前路由，false时没有删除按钮
        delete?: string | false;
        // 点击添加按钮时跳转到哪个路由,默认：当前路由/post?id=0,false时不显示添加按钮
        add?: string | false;
        // 点击编辑按钮时跳转到哪个路由,默认：当前路由/post?id=[id],false时不显示编辑按钮
        edit?: string | false;
    };
    // 按钮配置
    btns?: KBuilderAction[];
    // 分页配置
    page?: {
        limit?: number;
    };
}

export type propCallback = (val: any) => string;

/**
 * 表列
 */
export interface KBuilderColumn {
    type?: 'selection' | undefined;
    label?: string;
    prop?: string | propCallback;
    width?: string | number;
    align?: 'left' | 'center' | 'right';
}

/**
 * 表操作按钮
 */
export interface KBuilderAction {
    label?: string;
    onClick?: (props: KBuilderActionCacllback) => void;
}

export interface KBuilderActionCacllback {
    row: any;
    update: (newData: any) => void;
    delete: () => void;
}

/**
 * 接口返回
 */
export interface KBuilderResponse {
    total: number;
    items: any[];
}
