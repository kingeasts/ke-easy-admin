import { ConfigOption, OptionCallback } from './form';

export type FormComponent = FormInput |
    FormCheckbox |
    FormDate |
    FormRadio |
    FormSelect |
    FormImage |
    FormTextarea |
    FormMarkdown

export interface BaseFormItem {
    // 标签名
    label: string;
    // 标签宽度
    labelWidth?: string;
    // key
    prop: string;
    // 默认值：如果是多选，需传入数组
    defaultValue?: any;
    // 是否必填
    required?: boolean;
    // 没有值时显示的文本，部分组件无效
    placeholder?: string;
    // 控件显示条件
    visibleCondition?: (formdata: any, prop: string) => boolean;
}

export interface FormInput extends BaseFormItem {
    __obj__?: 'input';
}

export interface FormCheckbox extends BaseFormItem {
    __obj__?: 'checkbox';
    options?: ConfigOption[];
    optionLoad?: OptionCallback;
}

export interface FormRadio extends BaseFormItem {
    __obj__?: 'radio';
    options?: ConfigOption[];
    optionLoad?: OptionCallback;
}

export interface FormSelect extends BaseFormItem {
    __obj__?: 'select';
    options?: ConfigOption[];
    optionLoad?: OptionCallback;
}

export interface FormDate extends BaseFormItem {
    __obj__?: 'date';
    // 显示日期格式
    format?: string;
    // 值格式，如果为空则使用显示格式
    value_format?: string;
    // 显示类型
    type?: 'year' | 'month' | 'date' | 'dates' | 'week' | 'datetime' | 'datetimerange' | 'daterange' | 'monthrange';
    // 范围选择时开始日期的占位内容
    startPlaceholder?: string;
    // 范围选择时结束日期的占位内容
    endPlaceholder?: string;
}

export interface FormImage extends BaseFormItem {
    __obj__?: 'image';
    // 是否多选
    muitl?: boolean;
}

export interface FormTextarea extends BaseFormItem {
    __obj__?: 'textarea';
    // 行数
    rows?: number;
}

export interface FormMarkdown extends BaseFormItem {
    __obj__?: 'markdown';
}
