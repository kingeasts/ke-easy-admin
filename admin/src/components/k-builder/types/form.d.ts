export interface ConfigOption {
    label: string;
    value: string | number;
}

export type DataInitCallback = () => Promise<any>;

export interface KBuilderConfig {
    // 表单标题
    title?: string;

    // 全局标签宽度
    labelWidth?: string;

    // 是否自动处理未输入时的文本, default: true
    autoPlaceholder?: boolean;

    // 关闭表单验证,关闭后所有控件的required会失效 default: false
    closeValidate?: boolean;

    // 表单提交url
    submitUrl?: string;

    // 数据初始化
    initData?: string | DataInitCallback;
}

export type OptionCallback = () => Promise<ConfigOption[]>;
