export interface KTableState {
    loading: boolean;
}

export interface KTableQuery {
    page: number;
    limit: number;
}
