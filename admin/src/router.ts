import Vue from 'vue';
import VueRouter, { Route, RouteConfig } from 'vue-router';
import { setTitle } from '@/plugins/meta';
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'
import Layout from '@/layout/index/index.vue';
import { InitModule } from '@/store/initState';

NProgress.configure({ showSpinner: false })

Vue.use(VueRouter);

const files = require.context('./routes', true, /\.ts$/);
let routes: Array<RouteConfig> = [];
files.keys().forEach((file) => {
    routes = routes.concat(files(file).default)
});


const createRouter = () => new VueRouter({
    // mode: 'history', // require service support
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            sort: 100,
            module: 'index',
            component: Layout,
            children: [
                {
                    path: '',
                    name: 'Index',
                    sort: 200,
                    module: 'index',
                    meta: {
                        title: '首页'
                    },
                    component: () => import('@/views/index.vue')
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            meta: {
                title: '后台登陆'
            },
            component: () => import('@/views/public/login.vue')
        },
        {
            path: '/edit-password',
            component: Layout,
            children: [
                {
                    path: '',
                    meta: {
                        title: '修改密码',
                        noCache: true,
                    },
                    component: () => import('@/views/public/password.vue')
                }
            ]
        },
        {
            path: '/my-info',
            component: Layout,
            children: [
                {
                    path: '',
                    meta: {
                        title: '个人信息',
                        noCache: true,
                    },
                    component: () => import('@/views/public/myinfo.vue')
                }
            ]
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            let cls = document.getElementsByClassName('main-wrap-content')
            if (cls.length) {
                cls[0].scrollTop = 0;
            }
            return { x: 0, y: 0 };
        }
    }
})

const router = createRouter();

router.beforeEach((to: Route, from: Route, next) => {
    NProgress.start();
    if (to.meta?.title) {
        setTitle(to.meta?.title);
    }

    if (to.name !== 'login') {
        InitModule.init();
    } else {
        InitModule.setLoading(false);
    }
    next();
    // if (to.meta?.title) {
    //     document.title = to.meta?.title + ' - ' + title;
    // } else {
    //     document.title = title;
    // }
});

router.afterEach((to: Route, from: Route) => {
    let main = document.getElementById('main');
    if (main) {
        main.scrollTop = 0;
    }
    NProgress.done();
})

export function resetRouter() {
    const newRouter: any = createRouter()
    if (newRouter.matcher) {
        (router as any).matcher = newRouter.matcher // reset router
    }
}

export default router;

export { routes }
