import { Action, getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';
import { getInitState } from '@/initState';
import log from 'loglevel';

@Module({ name: 'init_state', dynamic: true, namespaced: true, store })
export default class InitStore extends VuexModule {
    $isInitial: boolean = false;
    $loading: boolean = true;

    get loading(): boolean {
        return this.$loading;
    }

    @Mutation
    setLoading(val: boolean) {
        this.$loading = val;
    }

    @Mutation
    setInitial(val: boolean) {
        this.$isInitial = val;
    }

    @Action
    init() {
        if (this.$isInitial) {
            return;
        }
        this.context.commit('setInitial', true);
        try {
            getInitState().finally(() => {
                this.setLoading(false);
            })
        } catch (e) {
            log.error(e);
        }
    }
}

export const InitModule = getModule(InitStore);
