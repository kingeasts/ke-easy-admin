import { getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';

export interface DialogConfig {
    title: string;
    url: string;
    width?: string; // 窗体宽度
    labelWidth?: string; // 表单标签宽度
    type: string; // 窗体类型
    form?: {[key: string]: DialogFormItem[]};
    data?: {
        [key: string]: any,
    };
    callback: Function;
    drawer?: {
        direction?: 'rtl' | 'ltr' | 'ttb' | 'btt',
    };
}

export type requiredCallback = (value: any, form: any) => boolean;

export type placeholderCallback = (value: any, form: any) => string;

export interface DialogFormItem {
    // 标签文本
    label?: string;
    // 表单字段
    prop: string;
    // 提示信息
    placeholder?: string | placeholderCallback;
    // 控件类型
    type?: 'hidden'
        | 'input'
        | 'password'
        | 'textarea'
        | 'select'
        | 'image'
        | 'radio'
        | 'checkbox'
        | 'transfer';
    default?: number | string;
    options?: DialogFormItemOption[];
    config?: any;
    confirm?: string;
    required?: boolean | requiredCallback;
}

export interface DialogFormItemOption {
    text: string;
    value: number | string;
}

export interface KTableHeader {
    label: string;
    prop?: string;
    type?: 'default' | 'action' | 'image';
    actions?: KTableHeaderAction[];
    width?: number | string;
    height?: number;
    default?: string;
}

export interface KTableHeaderAction {
    label: string;
    type?: 'drawer' | 'confirm' | 'redirect' | 'tree';
    message?: string;
    action: string | KTableHeaderActionRedirect;
    config?: {
        width?: string,
        direction?: 'rtl' | 'ltr' | 'ttb' | 'btt',
    };
}

export interface KTableHeaderActionRedirect {
    name?: string;
    path?: string;
    query?: string[];
    params?: string[];
}

@Module({ name: 'dialog', dynamic: true, namespaced: true, store })
export default class DialogStore extends VuexModule {
    private _info = {} as DialogConfig;
    private _form: {[key: string]: DialogFormItem[]} = {};
    private _data = {} as { [key: string]: any };
    private _visible: boolean = false;

    public get visible(): boolean {
        return this._visible;
    }

    public get info(): DialogConfig {
        return this._info;
    }

    public get form(): DialogFormItem[] {
        let list: any[] = [];
        for (let formKey in this._form) {
            if (formKey === this._info.type) {
                list = this._form[formKey];
                break;
            } else if (formKey.indexOf('|') > -1) {
                const tmps = formKey.split('|');
                if (tmps.indexOf(this._info.type) > -1) {
                    list = this._form[formKey];
                    break;
                }
            }
        }

        return list;
    }

    public get data(): { [key: string]: any } {
        return this._data ?? {};
    }

    /**
     * 打开新窗体
     * title 窗体标题
     * url 接口地址
     * type 窗体类型
     * callback 提交回调，参数为接口返回数据
     * data 窗体表单默认数据
     * @param data
     */
    @Mutation
    public open(data: DialogConfig) {
        this._info = { ...data };
        if (data.form) {
            this._form = data.form;
        }
        this._data = { ...data.data };
        this._visible = true;
    }

    /**
     * 执行回调
     * @param data
     */
    @Mutation
    public callback(data: any) {
        this._info.callback(data);
    }

    @Mutation
    public setValue({ key, value }: { key: string, value: any }) {
        if (!this._data) {
            this._data = {};
        }
        this._data = {
            ...this._data,
            [key]: value,
        }
        console.log('wewe', key, this._data)
    }

    /**
     * 关闭窗体
     */
    @Mutation
    public close() {
        this._visible = false;
    }

    /**
     * 销毁当前窗体数据
     */
    @Mutation
    public dispose() {
        this._info = {} as any;
        this._form = {} as any;
        this._data = {} as any;
    }
}

export const DialogModule = getModule(DialogStore);
