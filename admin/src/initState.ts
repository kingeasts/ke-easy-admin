import router from '@/router';
import { QuickModule } from '@/store/quick';
import { SystemModule } from '@/store/system';
import { AuthModule } from '@/store/auth';
import { MenuModule } from '@/store/menu';

/**
 * 状态初始化
 */
export async function getInitState() {
    try {
        await AuthModule.getInfo();
        await Promise.all([
            SystemModule.getServerConfig()
        ]);
        QuickModule.init();

        SystemModule.setConfig({
            title: 'EasyAdmin'
        });

        MenuModule.initMenu({
            routes: AuthModule.routes,
            route: router.currentRoute,
            topNavs: [
                {
                    name: 'index',
                    text: '控制台',
                },
                {
                    name: 'system',
                    text: '系统',
                },
                {
                    name: 'web',
                    text: '网站前台',
                    url: 'https://www.baidu.com'
                }
            ]
        });
    } catch (e) {
        await router.push('/login');
    }
}
