import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// import D from 'vue-element-dev';
import './plugins';
import './directives';

import './assets/css/global.scss';

Vue.config.productionTip = false;

// Vue.use(D);

export const vm = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
