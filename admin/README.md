# easy-admin

## 安装依赖
```
yarn install
```

### 启动调试
```
yarn serve
```

### 构建资源
```
yarn build
```

### 其它说明
1. [表单构建类](./docs/README.md)
2. [表格构建类](./docs/table.md)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
