module.exports = {
    productionSourceMap: false,
    transpileDependencies: ['vuex-module-decorators'],
    devServer: {
        proxy: {
            '/api': {
                target: 'http://easy-admin.localhost',
                pathRewrite: {
                    '^/api': ''
                }
            },
            '/uploads': {
                target: 'http://easy-admin.localhost'
            }
        }
    },
    chainWebpack(config) {
        // it can improve the speed of the first screen, it is recommended to turn on preload
        // it can improve the speed of the first screen, it is recommended to turn on preload
        config.plugin('preload').tap(() => [
            {
                rel: 'preload',
                // to ignore runtime.js
                // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
                fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
                include: 'initial'
            }
        ])

        // when there are many pages, it will cause too many meaningless requests
        config.plugins.delete('prefetch')
    }
}
