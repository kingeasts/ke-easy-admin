## 表格构建类

1. 一个基础的模板

```
// table.tsx
import { Component, Mixins } from 'vue-property-decorator';
import { KBuilderTable } from '@/components/k-builder/table';

@Component
export default class Page extends Mixins(KBuilderTable) {

    protected build() {
        this.setConfig({
            url: {
                data: 'articles',
                add: 'article/post',
                edit: 'article/post',
                delete: 'article'
            }
        })
        this.addColumn({ label: 'ID', prop: 'id' })
            .addColumn({ label: '标题', prop: 'title' })
            .addColumn({ label: '关键词', prop: 'keyword' })
            .addColumn({ label: '创建时间', prop: 'create_time' })
            .addColumn({ label: '更新时间', prop: 'update_time' })
    }

}
```

具体请看 [demo](../src/views/demo/table.tsx)
