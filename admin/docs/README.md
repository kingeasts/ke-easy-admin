# 表单构建类[tsx]

1. 一个基础的模板

```
// form.tsx
import { Component, Mixins } from 'vue-property-decorator';
import { KBuilderForm } from '@/components/k-builder/form';

@Component
export default class Page extends Mixins(KBuilderForm) {
    build() {
        this.addInput(...opt);
    }
}
```

具体请看 [demo](../src/views/demo/form.tsx)

> 输入框

```
this.addInput(...opt)
```

> 复选框

```
this.addCheckbox(...opt)
```

> 单选框

```
this.addRadio(...opt)
```

> 下拉框

```
this.addSelect(...opt)
```

> 日期选择

```
this.addDate(...opt)
```

> 图片上传

```
this.addImage(...opt)
```

> 文本域

```
this.addTextarea(...opt)
```

> Markdown

```
this.addMarkdown(...opt)
```
