# easy-admin

## 环境要求

php 7.4 (开发环境，7.2或许也可以)

mysql 5.7

## 后台界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/160212_477932d6_1643818.png "QQ截图20200719154836.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/160221_e83ff4ed_1643818.png "QQ截图20200719154850.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/160228_19bf665a_1643818.png "QQ截图20200719154900.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/160234_6460411c_1643818.png "QQ图片20200719154813.png")

## 克隆代码到本地

```
git clone https://gitee.com/kingeasts/ke-easy-admin.git
```


## 服务端初始化

* 复制server/.env.example到server/.env，并修改成自身的信息。

```
cd server
composer install
php think migrate:run
php think seed:run
kroute --watch
```

## 后台前端初始化

1. 修改admin/vue.config.js中的“http://easy-admin.localhost”为你服务端的地址

2. 初始化

```
cd admin
yarn
yarn serve
```

## 开发环境
```
cd admin
yarn serve
```

## 编译后台资源
```
cd admin
yarn build
```

## 后台注解路由
```
cd server

# 解析路由
kroute
# 监听路由解析
kroute --watch
```

## 控制器使用权限策略
```
// 用在类注释上为整个控制器生效
// 用在方法注释上为此方法生效
@UseAuth(policy="权限名")

// 不需要校验策略,优先级max
@NoAuth()
```
