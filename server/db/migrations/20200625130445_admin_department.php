<?php


use Phinx\Migration\AbstractMigration;

class AdminDepartment extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('admin_department', ['comment'=>'后台组织部门']);
        $table->addColumn('parent_id', 'integer', ['default'=>0, 'comment'=>'上级']);
        $table->addColumn('name', 'string', ['limit'=>32, 'comment'=>'名称']);
        $table->addColumn('create_time', 'integer');
        $table->addColumn('create_id', 'integer', ['comment'=>'创建人', 'default'=>0]);
        $table->save();

        $table = $this->table('admin');
        $table->addColumn('department_id', 'integer', ['default'=>0, 'comment'=>'部门']);
        $table->save();
    }
}
