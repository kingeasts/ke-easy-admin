<?php


use Phinx\Migration\AbstractMigration;

class UserFinanceLog extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('user_finance_log', ['comment'=>'用户财务日志']);
        $table->addColumn('user_id', 'integer', ['comment'=>'用户ID']);
        $table->addColumn('type', 'string', ['comment'=>'货币类型', 'limit'=>32]);
        $table->addColumn('value', 'decimal', ['comment'=>'数额']);
        $table->addColumn('remark', 'string', ['comment'=>'备注信息', 'limit'=>255]);
        $table->addColumn('create_time', 'integer');
        $table->addIndex(['user_id', 'type']);
        $table->save();
    }
}
