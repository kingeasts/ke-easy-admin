<?php


use Phinx\Migration\AbstractMigration;

class Article extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('article', ['comment'=>'文章']);
        $table->addColumn('cate_id', 'integer');
        $table->addColumn('title', 'string', ['limit'=>128, 'comment'=>'标题']);
        $table->addColumn('subtitle', 'string', ['limit'=>255, 'comment'=>'副标题']);
        $table->addColumn('keyword', 'string', ['limit'=>255, 'default'=>'', 'comment'=>'关键字']);
        $table->addColumn('content', 'text', ['comment'=>'文章内容']);
        $table->addColumn('create_time', 'integer');
        $table->addColumn('update_time', 'integer');
        $table->addColumn('view_count', 'integer', ['default'=>0, 'comment'=>'浏览量']);
        $table->addIndex(['cate_id']);
        $table->save();

        $table = $this->table('article_category', ['comment'=>'文章分类']);
        $table->addColumn('pid', 'integer', ['default'=>0, 'comment'=>'上级ID']);
        $table->addColumn('path', 'string', ['limit'=>255, 'comment'=>'分类路径']);
        $table->addColumn('name', 'string', ['limit'=>255, 'comment'=>'分类名']);
        $table->addColumn('sort', 'integer', ['default'=>0]);
        $table->save();
    }
}
