<?php


use Phinx\Migration\AbstractMigration;
use \Phinx\Db\Adapter\MysqlAdapter;

class Users extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('users', ['comment'=>'用户']);
        $table->addColumn('username', 'string', [
            'limit'=>64,
            'comment'=>'用户名'
        ])
            ->addColumn('password', 'string', [
                'limit'=>255,
                'comment'=>'密码'
            ])
            ->addColumn('nickname', 'string', [
                'limit'=>128,
                'comment'=>'昵称'
            ])
            ->addColumn('avatar', 'string', [
                'limit'=>255,
                'comment'=>'头像'
            ])
            ->addColumn('sex', 'boolean', [
                'comment'=>'性别：0女，1男',
                'default'=>0
            ])
            ->addColumn('age', MysqlAdapter::PHINX_TYPE_SMALL_INTEGER, [
                'comment'=>'性别：0女，1男'
            ])
            ->addColumn('email', 'string', [
                'limit'=>128,
                'comment'=>'邮件地址'
            ])
            ->addColumn('mobile', 'string', [
                'limit'=>11,
                'comment'=>'手机号码'
            ])
            ->addColumn('status', 'boolean', [
                'comment'=>'状态：0可用，1锁定'
            ])
            ->addColumn('money', MysqlAdapter::PHINX_TYPE_DECIMAL, [
                'comment'=>'余额',
                'precision'=>10,
                'scale'=>2,
                'signed'=>false
            ])
            ->addColumn('score', 'integer', [
                'comment'=>'积分',
                'signed'=>false
            ])
            ->addColumn('create_time', 'integer')
            ->addColumn('update_time', 'integer')
        ;
        $table->save();
    }
}
