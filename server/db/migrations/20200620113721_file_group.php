<?php


use Phinx\Migration\AbstractMigration;

class FileGroup extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('file_group', ['comment'=>'文件分组']);
        $table->addColumn('key', 'string', ['limit'=>32, 'comment'=>'文件夹名称，英文']);
        $table->addColumn('name', 'string', ['limit'=>32, 'comment'=>'分组名称']);
        $table->addColumn('create_time', 'integer');
        $table->addColumn('update_time', 'integer');
        $table->save();
    }
}
