<?php


use Phinx\Migration\AbstractMigration;

class File extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('file', ['comment'=>'上传文件列表']);
        $table->addColumn('group_id', 'integer', ['comment'=>'分组']);
        $table->addColumn('type', 'string', ['limit'=>32, 'comment'=>'类型：image图片，file文件']);
        $table->addColumn('filename', 'string', ['limit'=>255, 'comment'=>'文件名']);
        $table->addColumn('mime', 'string', ['limit'=>64, 'comment'=>'MIME']);
        $table->addColumn('src', 'string', ['limit'=>255, 'comment'=>'文件储存相对位置']);
        $table->addColumn('create_time', 'integer');
        $table->save();
    }
}
