<?php


use Phinx\Migration\AbstractMigration;

class AdminActionLog extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('admin_action_log', ['comment'=>'后台操作日志']);
        $table->addColumn('admin_id', 'integer', ['comment'=>'管理ID']);
        $table->addColumn('controller', 'string', ['comment'=>'控制器', 'limit'=>255]);
        $table->addColumn('message', 'string', ['comment'=>'操作说明', 'limit'=>255]);
        $table->addColumn('ip', 'biginteger', ['comment'=>'操作IP']);
        $table->addColumn('agent', 'string', ['limit'=>500]);
        $table->addColumn('create_time', 'integer', ['comment'=>'操作时间']);
        $table->addColumn('is_deleted', 'boolean', ['default'=>0, 'comment'=>'是否已删除,删除的数据会重新被覆盖']);
        $table->save();
    }
}
