<?php


use Phinx\Migration\AbstractMigration;

class Config extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('config', ['comment'=>'系统配置']);
        $table->addColumn('grouping', 'string', ['limit'=>32, 'comment'=>'分组']);
        $table->addColumn('key', 'string', ['limit'=>32, 'comment'=>'节点名']);
        $table->addColumn('title', 'string', ['limit'=>32, 'comment'=>'文本']);
        $table->addColumn('description', 'string', [
            'limit'=>255,
            'comment'=>'描述',
            'default'=>''
        ]);
        $table->addColumn('component', 'string', ['limit'=>32, 'comment'=>'组件类型']);
        $table->addColumn('data_type', 'string', ['limit'=>32, 'comment'=>'数据类型：int string float json', 'default'=>'']);
        $table->addColumn('visible_condition', 'string', [
            'limit'=>255,
            'null'=>true,
            'comment'=>'显示条件：basic.status=1,basic.status=0,basic.status!10'
        ]);
        $table->addColumn('sort', 'integer', ['default'=>0]);
        $table->addColumn('value', 'text', ['comment'=>'存储值']);
        $table->addColumn('options', 'json', ['comment'=>'预选值', 'null'=>true]);
        $table->addColumn('extends', 'json', ['comment'=>'扩展配置', 'null'=>true]);
        $table->save();
    }
}
