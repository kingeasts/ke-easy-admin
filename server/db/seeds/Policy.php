<?php


use Phinx\Seed\AbstractSeed;
use ke\auth\model\KePolicy;

class Policy extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $table = $this->table('admin_policy');
        $table->truncate();
        $map = [
            ['name'=>'Acl', 'text'=>'访问控制管理'],
            ['name'=>'Setting', 'text'=>'系统配置管理'],
            ['name'=>'User', 'text'=>'用户管理'],
            ['name'=>'UserRecharge', 'text'=>'用户充值'],
            ['name'=>'UserFinance', 'text'=>'用户财务日志'],
            ['name'=>'Article', 'text'=>'文章管理'],
            ['name'=>'ArticleCategory', 'text'=>'文章分类管理'],
            ['name'=>'Upload', 'text'=>'图片上传'],
            ['name'=>'AdminActionLog', 'text'=>'后台操作日志'],
        ];
        \think\Db::startTrans();
        try {
            foreach ($map as $item) {
                $v = KePolicy::where('name', $item['name'])->value('id');
                if (!$v) {
                    KePolicy::create($item);
                }
            }
            \think\Db::commit();
        } catch (Exception $e) {
            \think\Db::rollback();
        }
    }
}
