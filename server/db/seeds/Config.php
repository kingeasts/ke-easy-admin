<?php


use Phinx\Seed\AbstractSeed;

class Config extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $this->getAdapter()->truncateTable('config');

        $table = $this->table('config');
        $table->insert([
            [
                'grouping'=>'',
                'component'=>'group',
                'key'=>'basic',
                'title'=>'基础配置',
                'description'=>'',
                'sort'=>100,
                'value'=>'',
            ],
            [
                'grouping'=>'basic',
                'key'=>'status',
                'component'=>'radio',
                'title'=>'站点状态',
                'data_type'=>'int',
                'description'=>'',
                'sort'=>100,
                'value'=>0,
                'options'=>json_encode([
                    [
                        'value'=>0,
                        'text'=>'关闭'
                    ],
                    [
                        'value'=>1,
                        'text'=>'开启'
                    ],
                ])
            ],
            [
                'grouping'=>'basic',
                'key'=>'close_message',
                'component'=>'input',
                'title'=>'关闭原因说明',
                'data_type'=>'string',
                'description'=>'',
                'sort'=>100,
                'value'=>'站点维护中...',
                'visible_condition'=>'basic.status=0'
            ],
            [
                'grouping'=>'basic',
                'key'=>'title',
                'component'=>'input',
                'data_type'=>'string',
                'title'=>'站点标题',
                'description'=>'',
                'sort'=>100,
                'value'=>'easy-admin',
            ],
        ]);
        $table->insert([
            [
                'grouping'=>'',
                'key'=>'wechat',
                'component'=>'group',
                'title'=>'微信公众号',
                'description'=>'',
                'sort'=>200,
                'value'=>'',
            ],
            [
                'grouping'=>'wechat',
                'key'=>'appid',
                'component'=>'input',
                'data_type'=>'string',
                'title'=>'APPID',
                'description'=>'',
                'sort'=>100,
                'value'=>'',
            ],
            [
                'grouping'=>'wechat',
                'key'=>'secret',
                'component'=>'input',
                'data_type'=>'string',
                'title'=>'SECRET',
                'description'=>'',
                'sort'=>200,
                'value'=>'',
            ],
        ]);
        $table->save();
    }
}
