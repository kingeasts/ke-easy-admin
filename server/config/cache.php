<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

use think\facade\Config;

return [
    // 驱动方式
    'type'   => 'complex',
    // 默认使用redis
    'default'   =>  [
        // 驱动方式
        'type'   => 'redis',
        'host'       => env('redis_host', '127.0.0.1'),
        'port'       => env('redis_port', '6379'),
        'password'   => env('redis_pass'),
        'select'     => env('redis_select', 0),
        'timeout'    => env('redis_timeout', 0),
        'expire'     => 0,
        'persistent' => false,
        'prefix'     => Config::get('app_name', 'ea') . ':',
        'serialize'  => true,
    ],
    // 文件缓存
    'file'   =>  [
        // 驱动方式
        'type'   => 'file',
        // 全局缓存有效期（0为永久有效）
        'expire'=>  0,
        // 缓存保存目录
        'path'   => '../data/cache',
    ],
];
