<?php
return [
    'environments'=>[
        'default_migration_table'=>'phinxlog',
        'default_database'=>'default',
        'default'=>[
            'adapter'=>'mysql',
            'host'=>env('db_host'),
            'name'=>env('db_name'),
            'user'=>env('db_user'),
            'pass'=>env('db_pass'),
            'table_prefix'=>env('db_prefix'),
            'port'=>env('db_port'),
            'charset'=>'utf8mb4',
            'collation'=>'utf8mb4_unicode_ci'
        ]
    ]
];
