<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 带单位的文件大小字符串转换成字节数
 *
 * @param float|int $value 容量
 * @param string $unit 单位：b,kb,mb,gb,tb
 *
 * @return float|int
 */
function string_size_to_bytes($value, $unit = '')
{
    $unit = strtolower($unit);
    $unit = preg_replace('/[^a-z]/', '', $unit);

    $Units = ['b' => 0, 'kb' => 1, 'mb' => 2, 'gb' => 3, 'tb' => 4];
    $Exponent = isset($Units[$unit]) ? $Units[$unit] : 0;

    return ($value * pow(1024, $Exponent));
}
