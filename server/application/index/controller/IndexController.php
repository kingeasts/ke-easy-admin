<?php


namespace app\index\controller;


use ke\graphql\GraphQl;
use think\Request;

class IndexController
{
    public function index(Request $request)
    {
        $gr = new GraphQl();

        return json($gr->send($request->post('query')));
    }

}