<?php


namespace app\admin\annotation;


use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class UseAuth
 * @package app\admin\annotation
 * @Annotation
 * @Target({"METHOD", "CLASS"})
 */
final class UseAuth
{
    /**
     * @var string
     */
    public $policy;
}
