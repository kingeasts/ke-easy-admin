<?php


namespace app\admin\annotation;


/**
 * Class NoAuth
 * @package app\admin\annotation
 * @Annotation
 * @Target({"METHOD"})
 */
final class NoAuth
{

}