<?php


namespace app\admin\annotation;


/**
 * Class NoLogin
 * @package app\admin\annotation
 * @Annotation
 * @Target({"METHOD", "CLASS"})
 */
class NoLogin
{

}