<?php


namespace app\admin\types;


use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class LoginAdminInfoType extends ObjectType
{
    public function __construct()
    {
        parent::__construct([
            'name'=>'LoginAdminInfo',
            'fields'=>[
                'access_token'=>Type::string(),
                'expire_in'=>Type::int(),
            ]
        ]);
    }

}
