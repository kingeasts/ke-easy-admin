<?php


namespace app\admin\library;


class ResultCode extends \SplEnum
{
    const SUCCESS = [0, 'OK'];

    const PARAM_INVALID = [1000, '参数错误'];

    const DATA_NOT_FOUND = [2000, '记录不存在'];

}
