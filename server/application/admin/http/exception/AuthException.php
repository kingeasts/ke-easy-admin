<?php


namespace app\admin\http\exception;


use GraphQL\Error\ClientAware;
use ke\auth\exception\AuthException as Exception;

class AuthException extends Exception implements ClientAware
{
    public function isClientSafe()
    {
        return true;
    }


    public function getCategory()
    {
        return 'auth';
    }

}
