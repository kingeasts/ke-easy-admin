<?php


namespace app\admin\http\exception;


use app\common\http\exception\NotFoundException;
use Exception;
use app\common\http\exception\AppException as AppError;
use ke\auth\exception\AuthException;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle;
use think\exception\ValidateException;
use think\facade\App;

class AppException extends Handle
{
    public function render(Exception $e)
    {
        if ($e instanceof \InvalidArgumentException
            || $e instanceof ValidateException
        ) {
            return json(['code'=>1, 'message'=>$e->getMessage()], 200);
        }
        if ($e instanceof DataNotFoundException || $e instanceof ModelNotFoundException) {
            return json(['code'=>1, 'message'=>'记录不存在'], 404);
        }
        if ($e instanceof NotFoundException) {
            return json(null, 404);
        }
        if ($e instanceof AuthException || $e instanceof AppError) {
            return json(['code'=>1, 'message'=>$e->getMessage()], 200);
        }


        $info = [];
        if (App::isDebug()) {
            $info = [
                'code'=>$e->getCode(),
                'message'=>$e->getMessage(),
                'line'=>$e->getLine(),
                'file'=>$e->getFile(),
                'trace'=>$e->getTrace()
            ];
        } else {
            $info['code'] = 1;
            $info['message'] = lang('system error');
        }

        return json($info, 500);
    }
}