<?php


namespace app\admin\controller;


use app\admin\annotation\UseAuth;
use app\common\http\exception\NotFoundException;
use app\common\library\FormDataValidate;
use app\common\model\Users;
use think\Db;

class UsersController extends BaseController
{
    /**
     * 用户列表
     * @return \think\Response|\think\response\Json
     * @throws \think\exception\DbException
     * @route('admin/users', 'get')
     *
     * @UseAuth(policy="User")
     */
    public function index()
    {
        $result = Users::withSearch(['key'])
            ->order('create_time', 'desc')
            ->paginate();

        return $result->render();
    }


    /**
     * 获取用户信息
     * @param $id
     * @return \think\Response|\think\response\Json
     * @throws NotFoundException
     * @route('admin/user/:id', 'get')
     * @UseAuth(policy="User")
     */
    public function read($id)
    {
        $data = Users::where('id', $id)->field('password', true)->find();
        if (!$data) {
            throw new NotFoundException();
        }
        return $this->success($data->toArray());
    }


    /**
     * 修改用户
     * @param $id
     * @return \think\Response|\think\response\Json
     * @throws NotFoundException
     * @route('admin/user/:id', 'post')
     * @UseAuth(policy="User")
     */
    public function post($id)
    {
        $form = new FormDataValidate([
            'username|用户名'=>'require|max:64',
            'nickname|昵称'=>'require|max:128',
            'avatar|头像'=>'require|max:255',
            'sex'=>'number',
            'age'=>'number',
            'email'=>'max:128',
            'mobile'=>'mobile',
            'status'=>'number',
            'money'=>'float',
            'score'=>'float',
        ]);

        $data = Users::where('id', $id)->find();
        if (!$data) {
            throw new NotFoundException();
        }
        $data->save($form->toArray());

        return $this->success($data->toArray());
    }


    /**
     * 用户充值
     * @param $id
     * @return \think\Response|\think\response\Json
     * @throws NotFoundException
     * @route('admin/user/:id/recharge', 'post')
     * @UseAuth(policy="User")
     */
    public function recharge($id)
    {
        $form = new FormDataValidate([
            'type'=>'require|in:money,score',
            'value'=>'require|number',
        ]);

        $data = Users::where('id', $id)->find();
        if (!$data) {
            throw new NotFoundException();
        }
        Db::transaction(function () use($data, $form) {
            $data[$form->type] += $form->value;
            $data->save();

            $data->finances()->save([
                'type'=>$form->type,
                'value'=>$form->value,
                'remark'=>$form->remark,
            ]);

            $this->recordActionLog('修改用户ID：' . $data->id);
        });

        return $this->success([
            'value'=>$data[$form->type]
        ]);
    }


    /**
     * 删除用户
     * @param $id
     * @return \think\Response|\think\response\Json
     * @throws NotFoundException
     * @throws \Exception
     * @route('admin/user/:id', 'delete')
     * @UseAuth(policy="User")
     */
    public function delete($id)
    {
        $data = Users::where('id', $id)->field('id')->find();
        if (!$data) {
            throw new NotFoundException();
        }
        Db::transaction(function () use($data) {
            $this->recordActionLog('删除用户ID：' . $data->id);
            $data->delete();
        });

        return $this->success();
    }

}
