<?php


namespace app\admin\controller;


use app\admin\annotation\UseAuth;
use app\admin\validate\ConfigNodeValidate;
use app\common\http\exception\AppException;
use app\common\library\FormDataValidate;
use app\common\model\Config;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\Response;

/**
 * Class ConfigController
 * @package app\admin\controller
 */
class ConfigController extends BaseController
{
    /**
     * 配置节点
     * @return Response|\think\response\Json
     *
     * @route('admin/config/nodes', 'get')
     * @UseAuth(policy="Setting")
     */
    public function index()
    {
        $data = Config::order('sort', 'asc')->select();

        return $this->success($data);
    }


    /**
     * 更新配置
     * @return Response|\think\response\Json
     *
     * @route('admin/config', 'post')
     * @UseAuth(policy="Setting")
     */
    public function post()
    {
        Db::transaction(function () {
            $form = $this->request->post('data');

            $this->recordActionLog('修改站点配置');

            $data = Config::where('id', 'in', array_keys($form))->select();
            foreach ($data as $item) {
                $item->save([
                    'value'=>$form[$item->id],
                ]);
            }

        });
        return $this->success();
    }


    /**
     * 更新节点
     * @return Response|\think\response\Json
     * @throws Exception
     *
     * @route('admin/config/node', 'post')
     * @UseAuth(policy="Setting")
     */
    public function node()
    {
        $form = new FormDataValidate(ConfigNodeValidate::class, null, [
            'value',
        ]);
        if (!$form->id) {
            $form->setDefault([
                'grouping'=>'',
                'sort'=>$_SERVER['REQUEST_TIME'],
                'value'=>'',
                'description'=>'',
            ]);
        }
        if (Config::where('grouping', $form->grouping)
            ->where('key', $form->key)
            ->where('id', '<>', $form->id)
            ->value('id')) {
            throw new AppException('节点重复');
        }
        if ($form->component === 'checkbox') {
            if ($form->value === '') {
                $form->value = [];
            } else if (!is_array($form->value)) {
                $form->value = [];
            }
        }

        if ($form->id) {
            $data = Config::where('id', $form->id)->findOrFail();
        } else {
            $data = new Config();
        }
        $data->save($form->toArray());
        $this->recordActionLog('修改配置节点');

        return $this->success($data);
    }


    /**
     * 删除节点
     * @return Response
     * @throws Exception
     *
     * @route('admin/config/node', 'delete')
     * @UseAuth(policy="Setting")
     */
    public function deleteNode()
    {
        $id = $this->request->post('ids/a', []);
        if (empty($id)) {
            throw new AppException('没有要删除的行');
        }

        $n = Config::where('id', 'in', $id)->delete();

        return $this->success(['count'=>$n]);
    }

}
