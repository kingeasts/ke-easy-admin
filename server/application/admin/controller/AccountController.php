<?php


namespace app\admin\controller;


use app\admin\annotation\NoLogin;
use app\common\http\exception\AppException;
use app\common\library\FormDataValidate;
use app\common\validate\AdminValidate;
use ke\auth\exception\AuthException;
use ke\auth\logic\Auth;
use think\facade\App;
use think\Request;

class AccountController extends BaseController
{

    /**
     * 登陆
     * @param Request $request
     * @return string|\think\Response|\think\response\Json
     * @throws AuthException
     * @route('admin/account/login', 'post')
     * @NoLogin()
     */
    public function login(Request $request)
    {
        $form = $request->only(['username', 'password', 'nickname'], 'post');
        $this->validate($form, AdminValidate::class, 'login');

        $auth = Auth::instance();
        $expire_in = 7200;
        $auth->login($form['username'], $form['password'], $expire_in);

        return $this->success([
            'access_token'=>$auth->loginToken(),
            'expire_in'=>$expire_in,
            'policys'=>$auth->getPolicys(),
        ]);
    }


    /**
     * 注销登陆
     * @route('admin/account/logout', 'post')
     */
    public function logout()
    {
        $this->auth->logout();

        return $this->success();
    }


    /**
     * 刷新令牌有效时长
     * @route('admin/account/refresh_token', 'post')
     * @throws AuthException
     */
    public function refreshToken()
    {
        $this->auth->refresh(7200);

        return $this->success([
            'expire_in'=>7200,
        ]);
    }


    /**
     * 获取当前登录信息
     * @route('admin/account/current', 'get')
     */
    public function getInfo()
    {
        $auth = Auth::instance();

        return $this->success([
            'info'=>$auth->getInfo(),
            'policys'=>$auth->getPolicys(),
        ]);
    }


    /**
     * 修改个人信息
     * @return \think\Response|\think\response\Json
     * @route('admin/account/edit_info', 'post')
     */
    public function editInfo()
    {
        $form = new FormDataValidate([
            'nickname|称呼'=>'require|max:32',
            'phone|手机号'=>'require|mobile',
        ]);

        $this->auth->save($form->toArray());

        return $this->success();
    }


    /**
     * 修改头像
     * @return \think\Response|\think\response\Json
     * @throws AppException
     * @route('admin/account/edit_avatar', 'post')
     */
    public function changeAvatar()
    {
        $file = $this->request->file('file');
        if (!$file) {
            throw new AppException('没有接收到图片数据');
        }

        $dir = sprintf('/uploads/admin/avatar/%d/', 1);

        $validate = [
            'size'=>string_size_to_bytes(2, 'mb'),
            'ext'=>'jpg,png,jpeg'
        ];
        $info = $file->validate($validate)->move(App::getRootPath() . $dir);
        if (!$info) {
            throw new AppException($file->getError());
        }

        $src = $dir . str_replace('\\', '/', $info->getSaveName());
        $this->auth->save([
            'avatar'=>$src
        ]);

        return $this->success([
            'src'=>$src,
            'filename'=>$file->getFilename(),
            'ext'=>$file->getExtension(),
        ]);
    }


    /**
     * 密码修改
     * @route('admin/account/edit_pass', 'post')
     */
    public function editPass()
    {
        $form = new FormDataValidate([
            'old|原密码'=>'require|max:32|alphaDash',
            'new|新密码'=>'require|max:32|alphaDash',
        ]);

        $this->auth->changePassword($form->old, $form->new);
        $this->auth->logout();

        return $this->success();
    }

}
