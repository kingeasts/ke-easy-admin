<?php


namespace app\admin\controller\upload;


use app\admin\annotation\UseAuth;
use app\admin\controller\BaseController;
use app\common\model\FileGroup;

/**
 * Class GroupController
 * @package app\admin\controller\upload
 * @UseAuth(policy="Upload")
 */
class GroupController extends BaseController
{
    /**
     * 文件分组
     * @return \think\Response|\think\response\Json
     * @route('admin/file/groups', 'get')
     */
    public function index()
    {
        $list = FileGroup::order([
            'update_time'=>'asc',
            'create_time'=>'asc'
        ])->select();

        return $this->success([
            'items'=>$list
        ]);
    }

}