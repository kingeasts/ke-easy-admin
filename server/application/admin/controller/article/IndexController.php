<?php


namespace app\admin\controller\article;


use app\admin\annotation\UseAuth;
use app\admin\controller\BaseController;
use app\admin\validate\ArticleValidate;
use app\common\http\exception\AppException;
use app\common\library\FormDataValidate;
use app\common\model\Article;
use think\Db;
use think\db\Query;

/**
 * Class IndexController
 * @package app\admin\controller\article
 * @UseAuth(policy="Article")
 */
class IndexController extends BaseController
{
    /**
     * @var Article
     */
    protected $model;

    protected function init()
    {
        $this->model = new Article();
    }

    /**
     * 文章列表
     * @return mixed
     * @route('admin/articles', 'get')
     */
    public function index()
    {
        $result = $this->model->autoList([
            'with'=>['cate'],
            'withBind'=>[
                'cate'=>['name']
            ],
            'ignore_field'=>['content'],
            'where'=>function (Query $query) {
                $cate_id = $this->request->get('cate_id/d');
                if (!empty($cate_id)) {
                    $query->whereIn('cate_id', $cate_id);
                }
            }
        ]);

        return $result->render();
    }

    /**
     * 新增 / 编辑文章
     * @return mixed
     * @route('admin/article/post', 'post')
     */
    public function post()
    {
        $data = $this->model->autoSave(ArticleValidate::class);

        return $this->success(null, $data);
    }


    /**
     * 文章详情
     * @param $id
     * @return mixed
     * @route('admin/article/:id', 'get')
     */
    public function read($id)
    {
        $result = $this->model->autoRead([
            'id'=>$id,
        ]);

        return $this->success($result);
    }



    /**
     * 删除文章
     * @return \think\Response|\think\response\Json
     * @route('admin/article', 'delete')
     */
    public function delete()
    {
        $this->model->autoDelete();

        return $this->success();
    }

}