<?php


namespace app\admin\controller\admin;


use app\admin\annotation\UseAuth;
use app\admin\controller\BaseController;
use ke\auth\model\KePolicy;

/**
 * Class PolicyController
 * @package app\admin\controller\admin
 * @UseAuth(policy="Acl")
 */
class PolicyController extends BaseController
{

    /**
     * 策略列表
     * @route('admin/policys', 'get')
     */
    public function index()
    {
        $list = KePolicy::order([
            'sort'=>'asc',
            'create_time'=>'asc'
        ])
            ->select();

        return $this->success([
            'items'=>$list
        ]);
    }

}
