<?php


namespace app\admin\controller\admin;


use app\admin\annotation\UseAuth;
use app\admin\controller\BaseController;
use app\common\model\AdminActionLog;
use think\db\Query;


/**
 * Class ActionLogController
 * @package app\admin\controller\admin
 * @UseAuth(policy="AdminActionLog")
 */
class ActionLogController extends BaseController
{
    /**
     * 操作日志列表
     * @return mixed
     * @throws \think\exception\DbException
     * @route('admin/action/logs', 'get')
     */
    public function index()
    {
        $result = AdminActionLog::with(['user'=>function (Query $query) {
            $query->field('id,nickname,avatar');
        }])
            ->where('is_deleted', 0)
            ->order('create_time', 'desc')
            ->paginate();

        return $result->render();
    }


    /**
     * 操作日志详情
     * @param $id
     * @return \think\Response|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @route('admin/action/log/:id', 'get')
     */
    public function read($id)
    {
        $data = AdminActionLog::where('id', $id)
            ->where('is_deleted', 0)
            ->findOrFail();

        return $this->success($data);
    }

}
