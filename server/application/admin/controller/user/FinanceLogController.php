<?php


namespace app\admin\controller\user;


use app\admin\annotation\UseAuth;
use app\admin\controller\BaseController;
use app\common\model\UserFinanceLog;
use think\db\Query;

/**
 * Class FinanceLogController
 * @package app\admin\controller\user
 */
class FinanceLogController extends BaseController
{
    /**
     * 用户财务日志列表
     * @return mixed
     * @throws \think\exception\DbException
     * @route('admin/user/finance_logs', 'get')
     *
     * @UseAuth(policy="UserFinance")
     */
    public function index()
    {
        $with = [
            'user'=>function (Query $query) {
                $query->field('id,nickname');
            }
        ];

        $result = UserFinanceLog::with($with)
            ->where(function (Query $query) {
            $user_id = $this->request->get('user_id/d');
            if ($user_id) {
                $query->where('user_id', $user_id);
            }
            $type = $this->request->get('type');
            if ($type) {
                $query->where('type', $type);
            }
        })
            ->order('create_time', 'desc')
            ->paginate();

        return $result->render();
    }

}