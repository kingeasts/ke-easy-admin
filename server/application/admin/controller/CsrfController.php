<?php


namespace app\admin\controller;


use app\admin\annotation\NoLogin;
use think\facade\Cache;
use think\facade\Cookie;

/**
 * Class CsrfController
 * @package app\admin\controller
 * @NoLogin()
 */
class CsrfController extends BaseController
{

    /**
     * 初始化csrf_token
     * @route('admin/csrf_token', 'get')
     */
    public function index()
    {
        $token = md5(mt_rand(0, 9999999) . uniqid($this->request->ip()));
        Cookie::set('XSRF-TOKEN', $token, 86400 * 7);
        Cache::set('xsrf-token:' . $token, 1, 86400 * 7);

        return $this->success();
    }

}