<?php

return [
    'system error'=>'系统错误',
    'username no exist'=>'用户 %s 不存在',
    'password required'=>'请填写密码',
    'password error'=>'密码错误',
];
