<?php


namespace app\admin\services;


use GraphQL\Type\Definition\ObjectType;
use think\Container;

class BaseService
{
    /**
     * @param $class
     * @return mixed
     */
    public function get($class)
    {
        return Container::get($class);
    }


    public function query()
    {
        return [];
    }


    public function mutations()
    {
        return [];
    }
}