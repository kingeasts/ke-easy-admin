<?php


namespace app\admin\services;


use app\admin\types\LoginAdminInfoType;
use app\admin\http\exception\AuthException as AppAuthException;
use GraphQL\Type\Definition\Type;
use ke\auth\exception\AuthException;
use ke\auth\logic\Auth;

class AdminService extends BaseService
{
    public function mutations()
    {
        $mutations['accountLogin'] = [
            'args'=>[
                'username'=>Type::nonNull(Type::string()),
                'password'=>Type::nonNull(Type::string()),
            ],
            'type'=>$this->get(LoginAdminInfoType::class),
            'resolve'=>function($val, $args) {
                $auth = Auth::instance();

                $expireIn = 7200;
                try {
                    $auth->login($args['username'], $args['password'], $expireIn);
                } catch (AuthException $e) {
                    throw new AppAuthException($e->getMessage());
                }

                return [
                    'access_token'=>$auth->loginToken(),
                    'expire_in'=>$expireIn,
                ];
            },
        ];

        return $mutations;
    }

}