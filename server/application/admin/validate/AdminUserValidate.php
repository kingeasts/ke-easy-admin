<?php


namespace app\admin\validate;


use think\Validate;

class AdminUserValidate extends Validate
{
    public $rule = [
        'id'=>'require|number',
        'avatar|头像'=>'require|max:255',
        'nickname|名字'=>'require|max:64',
        'username|用户'=>'require|max:64',
        'password|密码'=>'require|max:64',
        'phone|手机号'=>'require|mobile',
        'roles|角色'=>'array',
        'department_id|部门'=>'number'
    ];


    public function sceneAdd()
    {
        return $this->remove('id');
    }

    public function sceneEdit()
    {
        return $this->remove('password');
    }
}
