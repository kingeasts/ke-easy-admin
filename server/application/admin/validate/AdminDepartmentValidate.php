<?php


namespace app\admin\validate;


use think\Validate;

class AdminDepartmentValidate extends Validate
{
    public $rule = [
        'id'=>'require|number',
        'parent_id|上级'=>'require|number',
        'name|名称'=>'require|max:32',
    ];

    public function sceneAdd()
    {
        return $this->remove('id');
    }

}