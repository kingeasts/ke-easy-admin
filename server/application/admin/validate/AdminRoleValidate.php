<?php


namespace app\admin\validate;


use think\Validate;

class AdminRoleValidate extends Validate
{
    public $rule = [
        'id'=>'require|number',
        'name|角色名称'=>'require|max:32',
        'remark|备注'=>'max:128',
        'permission|权限'=>'array'
    ];

    public function sceneAdd()
    {
        return $this->remove('id');
    }

}
