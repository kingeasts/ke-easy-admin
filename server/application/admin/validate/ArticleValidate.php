<?php


namespace app\admin\validate;


use think\Validate;

class ArticleValidate extends Validate
{
    public $rule = [
        'id'=>'number',
        'cate_id'=>'require|number',
        'title|标题'=>'require|max:128',
        'subtitle|副标题'=>'require|max:255',
        'keyword|关键字'=>'max:255',
        'content|文章内容'=>'require',
    ];

    protected $message = [
        'cate_id'=>'请选择分类',
    ];

    public function sceneAdd()
    {
        return $this->remove('id');
    }
}
