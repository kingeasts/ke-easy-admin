<?php


namespace app\admin\validate;


use think\Validate;

class ArticleCategoryValidate extends Validate
{
    public $rule = [
        'id'=>'require|number',
        'pid'=>'number',
        'name|分类名称'=>'require|max:128',
        'sort'=>'number'
    ];

    public function sceneAdd()
    {
        return $this->remove('id');
    }
}