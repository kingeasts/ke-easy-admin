<?php


namespace app\admin\validate;

use think\Validate;

class ConfigNodeValidate extends Validate
{
    public $rule = [
        'id'=>'number',
        'grouping|分组'=>'max:32|alphaDash',
        'key|节点'=>'require|max:32|alphaDash',
        'title|文本'=>'require|max:32',
        'description|描述'=>'max:255',
        'component|组件类型'=>'require|max:32|checkComponent',
        'data_type|数据类型'=>'require|max:32|in:string,int',
        'visible_condition|显示条件'=>'max:255',
        'options|预选值'=>'checkOption',
        'sort|顺序'=>'number',
    ];


    protected function checkComponent($value, $rule, $data = [])
    {
        if (in_array($value, ['checkbox', 'radio', 'select']) && empty($data['options'])) {
            return '必须配置预选值';
        }
        return true;
    }


    protected function checkOption($value, $rule, $data = [])
    {
        if (empty($value)) {
            return true;
        }
        foreach ($value as $i=>$v) {
            if (empty($v['text'])) {
                return $i . '行text为空';
            }
            if (empty($v['value'])) {
                return $i . '行value为空';
            }
        }

        return true;
    }
}
