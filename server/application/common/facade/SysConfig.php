<?php
// +----------------------------------------------------------------------
// | Auth: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\facade;


use think\Facade;

/**
 * Class SysConfig
 * @package app\common\library\facade
 * @method static string|array get(string $key = null)
 */
class SysConfig extends Facade
{
    protected static function getFacadeClass()
    {
        return \app\common\library\SysConfig::class;
    }

}
