<?php


namespace app\common\library;


use ke\utils\FormData;
use think\exception\ValidateException;

class FormDataValidate extends FormData
{
    public function __construct($rule, string $scene = null, array $extends = [])
    {
        parent::__construct($rule, $scene);
        if (count($extends)) {
            $this->extend($extends);
        }
        $this->check();
    }

    public function check($data = null, $b = null, $c = null)
    {
        $chk = parent::check($data);
        if (!$chk) {
            throw new ValidateException($this->getError());
        }
        return true;
    }
}
