<?php
// +----------------------------------------------------------------------
// | Auth: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\library;


use app\common\model\Config;

class SysConfig
{
    private $config = [];


    public function __construct()
    {
        $list = Config::order('sort', 'asc')->select();
        $this->config = $list->toArray();
        $this->config = [];
        foreach ($list as $item) {
            if ($item->grouping === '') {
                $this->config[$item->key] = [];
            }
        }

        foreach ($list as $item) {
            if (isset($this->config[$item->grouping])) {
                $this->config[$item->grouping][$item->key] = $item->value;
            }
        }
    }


    /**
     * 获取节点
     * @param null|string $key 支持.获取子节点
     * @return array|mixed|null
     */
    public function get($key = null)
    {
        if (is_null($key)) {
            return $this->config;
        }
        if (strpos($key, '.') === false) {
            return $this->config[$key] ?? null;
        }
        $tmp = explode('.', $key);
        $list = $this->config;
        foreach ($tmp as $tag) {
            if (isset($list[$tag])) {
                $list = $list[$tag];
            } else {
                $list = null;
                break;
            }
        }
        return $list;
    }

}
