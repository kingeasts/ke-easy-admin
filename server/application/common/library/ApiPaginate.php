<?php


namespace app\common\library;


use app\common\traits\Jump;
use think\Paginator;

class ApiPaginate extends Paginator
{
    use Jump;

    public function __construct($items, $listRows, $currentPage = null, $total = null, $simple = false, $options = [])
    {
        parent::__construct($items, $listRows, $currentPage, $total, $simple, $options);
    }

    public function render()
    {
        return $this->success([
            'items'=>$this->items(),
            'total'=>$this->total(),
            'limit'=>$this->listRows(),
            'has_more'=>$this->hasMore
        ]);
    }
}
