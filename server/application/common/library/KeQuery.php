<?php


namespace app\common\library;


use think\Container;
use think\db\Query;

class KeQuery extends Query
{
    public function paginate($listRows = null, $simple = false, $config = [])
    {
        if (is_null($listRows)) {
            $app = Container::get('app');
            $listRows = $app->request->get('limit', 10, 'intval');
        }
        return parent::paginate($listRows, $simple, $config);
    }

}
