<?php


namespace app\common\http\exception;


use ke\auth\exception\AuthException;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\ValidateException;
use think\facade\App;

class AppException extends Exception
{
    public function render(Exception $e)
    {
        if ($e instanceof \InvalidArgumentException
            || $e instanceof ValidateException
        ) {
            return json(['code'=>1, 'message'=>$e->getMessage()], 200);
        }
        if ($e instanceof DataNotFoundException || $e instanceof ModelNotFoundException) {
            return json(['code'=>1, 'message'=>'记录不存在'], 404);
        }
        if ($e instanceof NotFoundException) {
            return json(null, 404);
        }
        if ($e instanceof AuthException) {
            return json(['code'=>1, 'message'=>$e->getMessage()], 200);
        }

        $info = [];
        if (App::isDebug()) {
            $info = [
                'code'=>$e->getCode(),
                'message'=>$e->getMessage(),
                'line'=>$e->getLine(),
                'file'=>$e->getFile(),
                'trace'=>$e->getTrace()
            ];
        } else {
            $info['code'] = 1;
            $info['message'] = lang('system error');
        }

        return json($info, 500);
    }

}
