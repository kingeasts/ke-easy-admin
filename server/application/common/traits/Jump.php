<?php
/**
 * +----------------------------------------------------------------------
 * | Name: keAdmin
 * | Author King east To 1207877378@qq.com
 * +----------------------------------------------------------------------
 */


namespace app\common\traits;


use think\Response;
use think\response\Json;

trait Jump
{

    /**
     * 返回数据
     * @param int $httpCode
     * @param mixed $data
     * @return Json
     */
    protected function result($httpCode, $data = null): Response
    {
        if (is_null($data)) {
            return response('', $httpCode);
        }
        return json($data, $httpCode);
    }


    /**
     * 接口成功响应
     * @param null|array $data
     * @param array $extends
     * @return Response|Json
     */
    protected function success($data = null, array $extends = []): Response
    {
        $v = ['code'=>0];
        if (!is_null($data)) {
            $v['data'] = $data;
        }
        return $this->result(200, array_merge($v, $extends));
    }


    /**
     * 接口成功响应
     * @param string $message
     * @return Response|Json
     */
    protected function error($message): Response
    {
        $v = ['error'=>$message];
        return $this->result(200, $v);
    }

}
