<?php


namespace app\common\validate;


use think\Validate;

class AdminValidate extends Validate
{
    protected $rule = [
        'username|用户名'=>'require|max:32|alphaNum',
        'password|密码'=>'require|checkPassword',
        'nickname|昵称'=>'require|max:20',
        'phone|手机号码'=>'mobile',
        'status|状态'=>'accepted'
    ];


    /**
     * 登陆场景
     * @return AdminValidate
     */
    public function sceneLogin()
    {
        return $this->only([
            'username',
            'password'
        ]);
    }


    /**
     * 编辑场景
     * @return AdminValidate
     */
    public function sceneEdit()
    {
        return $this->remove('password', 'require');
    }


    /**
     * 密码校验
     * @param $value
     * @return bool|string
     */
    protected function checkPassword($value)
    {
        if (preg_match('/^[A-Za-z\d%\.\!@\#\$&\*:]+$/', $value)) {
            return true;
        }
        return '密码只能为字母、数字、%.!@#$&*:组成';
    }
}
