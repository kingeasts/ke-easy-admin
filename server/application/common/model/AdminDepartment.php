<?php


namespace app\common\model;


use ke\Model;

class AdminDepartment extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $type = [
        'id'=>'integer',
        'parent_id'=>'integer',
        'create_id'=>'integer'
    ];

}
