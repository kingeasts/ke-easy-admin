<?php


namespace app\common\model;


use ke\auth\model\KeUser;
use ke\Model;

class AdminActionLog extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $type = [
        'id'=>'integer',
        'is_deleted'=>'boolean'
    ];


    public function user()
    {
        return $this->hasOne(KeUser::class, 'id', 'admin_id');
    }
}
