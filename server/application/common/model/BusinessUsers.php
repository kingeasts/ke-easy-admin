<?php


namespace app\common\model;


use ke\Model;

class BusinessUsers extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $type = [
        'id'=>'integer',
        'business_id'=>'integer',
        'status'=>'integer',
        'login_time'=>'timestamp',
        'login_count'=>'integer',
        'login_fail_time'=>'timestamp',
        'login_fail_count'=>'integer',
    ];

    /**
     * 商户信息
     * @return \think\model\relation\HasMany
     */
    public function business()
    {
        return $this->hasMany(Business::class, 'id', 'have_id');
    }

}
