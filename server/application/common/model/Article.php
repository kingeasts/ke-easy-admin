<?php


namespace app\common\model;


use ke\KeAutoLogic;
use ke\Model;
use think\db\Query;
use think\facade\Request;

class Article extends Model
{
    use KeAutoLogic;

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $type = [
        'id'=>'integer',
        'cate_id'=>'integer',
        'view_count'=>'integer',
    ];

    public function cate()
    {
        return $this->hasOne(ArticleCategory::class, 'id', 'cate_id');
    }

    public function searchTitleAttr(Query $query, $value, $data)
    {
        $key = Request::get('key');
        if ($key) {
            $query->where(function (Query $query) use($key) {
                $query->where('title', 'like', $key . '%');
            });
        }
    }
}
