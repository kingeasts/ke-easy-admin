<?php


namespace app\common\model;


use ke\Model;

class Business extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $type = [
        'id'=>'integer',
        'have_id'=>'integer',
        'status'=>'integer'
    ];
    protected $json = [
        'services'
    ];
    protected $jsonAssoc = true;

    /**
     * 员工列表
     * @return \think\model\relation\HasMany
     */
    public function users()
    {
        return $this->hasMany(BusinessUsers::class, 'business_id', 'id');
    }

}
