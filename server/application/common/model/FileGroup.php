<?php


namespace app\common\model;


use ke\Model;

class FileGroup extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $type = [
        'id'=>'integer'
    ];
}
