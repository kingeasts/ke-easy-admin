<?php


namespace app\common\model;


use ke\Model;

/**
 * Class UserFinanceLogModel
 * @package app\common\model
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property float $value
 * @property string $remark
 * @property string $create_time
 */
class UserFinanceLog extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $type = [
        'id'=>'integer',
        'user_id'=>'integer',
        'value'=>'float'
    ];

    /**
     * 关联用户
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }

}