<?php


namespace app\common\model;


use ke\Model;

class ArticleCategory extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $type = [
        'id'=>'integer',
    ];
}
