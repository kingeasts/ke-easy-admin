<?php


namespace app\common\model;


use ke\Model;

class File extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $type = [
        'id'=>'integer',
        'group_id'=>'integer'
    ];

}
