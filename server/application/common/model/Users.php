<?php


namespace app\common\model;


use ke\Model;
use think\db\Query;
use think\facade\Request;

/**
 * Class UsersModel
 * @package app\common\model
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $nickname
 * @property string $avatar
 * @property int $sex
 * @property int $age
 * @property string $email
 * @property string $mobile
 * @property int $status
 * @property float $money
 * @property int $score
 * @property string $create_time
 * @property string $update_time
 */
class Users extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $type = [
        'money'=>'float',
        'score'=>'integer'
    ];

    public function searchKeyAttr(Query $query, $value, $data)
    {
        $key = Request::get('key');
        if ($key) {
            $query->where(function (Query $query) use($key) {
                $query->where('id|mobile|email', $key)
                    ->whereOr('nickname', 'like', "{$key}%");
            });
        }
    }


    /**
     * 关联财务日志
     * @return \think\model\relation\HasMany
     */
    public function finances()
    {
        return $this->hasMany(UserFinanceLog::class, 'user_id', 'id');
    }
}
