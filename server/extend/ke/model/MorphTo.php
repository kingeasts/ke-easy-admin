<?php
/**
 * +----------------------------------------------------------------------
 * | Name: juhong
 * | Author King east To 1207877378@qq.com
 * +----------------------------------------------------------------------
 */

namespace ke\model;


use think\Exception;
use think\Loader;

/**
 * Class MorphTo
 * 修复官方多态关联无法限制字段
 * @package ke\model
 */
class MorphTo extends \think\model\relation\MorphTo
{

    /**
     * 预载入关联查询
     * @access public
     * @param  array    $resultSet   数据集
     * @param  string   $relation    当前关联名
     * @param  string   $subRelation 子关联名
     * @param  \Closure $closure     闭包
     * @return void
     * @throws Exception
     */
    public function eagerlyResultSet(&$resultSet, $relation, $subRelation, $closure)
    {
        $morphKey  = $this->morphKey;
        $morphType = $this->morphType;
        $range     = [];

        foreach ($resultSet as $result) {
            // 获取关联外键列表
            if (!empty($result->$morphKey)) {
                $range[$result->$morphType][] = $result->$morphKey;
            }
        }

        // 关联属性名
        if (!empty($range)) {
            $attr = Loader::parseName($relation);
            foreach ($range as $key => $val) {
                // 多态类型映射
                $model = $this->parseModel($key);
                $obj   = (new $model)->db();
                $pk    = $obj->getPk();
                // 预载入关联查询 支持嵌套预载入
                if ($closure instanceof \Closure) {
                    $closure($obj);

                    if ($field = $obj->getOptions('with_field')) {
                        $obj->field($field)->removeOption('with_field');
                    }
                }
                $list  = $obj->all($val, $subRelation);
                $data  = [];

                foreach ($list as $k => $vo) {
                    $data[$vo->$pk] = $vo;
                }

                foreach ($resultSet as $result) {
                    if ($key == $result->$morphType) {
                        // 关联模型
                        if (!isset($data[$result->$morphKey])) {
                            $relationModel = null;
                        } else {
                            $relationModel = $data[$result->$morphKey];
                            $relationModel->setParent(clone $result);
                            $relationModel->isUpdate(true);
                        }

                        $result->setRelation($attr, $relationModel);
                    }
                }
            }
        }
    }
}