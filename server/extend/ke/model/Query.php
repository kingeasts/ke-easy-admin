<?php
// +----------------------------------------------------------------------
// | gxy_core
// +----------------------------------------------------------------------
// | User: wzdon
// +----------------------------------------------------------------------
// | Author: king east <1207877378@qq.com>
// +----------------------------------------------------------------------

namespace ke\model;

use think\facade\Request;
use think\Paginator;

/**
 * Class Query
 * @package ke\model
 */
abstract class Query extends \think\db\Query
{
    /**
     * 分页器
     * @param null $listRows
     * @param bool $simple
     * @param array $config
     * @return $this[]|Paginator
     * @throws \think\exception\DbException
     */
    public function paginate($listRows = null, $simple = false, $config = [])
    {
        return parent::paginate($listRows, $simple, $config);
    }
}
