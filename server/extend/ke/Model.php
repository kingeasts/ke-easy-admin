<?php
/**
 * +----------------------------------------------------------------------
 * | Name: juhong
 * | Author King east To 1207877378@qq.com
 * +----------------------------------------------------------------------
 */


namespace ke;


use ke\model\MorphTo;
use ke\model\Query;
use think\Container;
use think\Loader;
use think\Paginator;


/**
 * Class Model
 * 修复官方多态关联无法限制字段问题
 * @package ke
 * @mixin Query
 * @method $this withSearch(array $fields, array $data = [], string $prefix = '') static 查询条件
 * @method $this where(mixed $field, string $op = null, mixed $condition = null) static 查询条件
 * @method $this whereRaw(string $where, array $bind = []) static 表达式查询
 * @method $this whereExp(string $field, string $condition, array $bind = []) static 字段表达式查询
 * @method $this when(mixed $condition, mixed $query, mixed $otherwise = null) static 条件查询
 * @method $this join(mixed $join, mixed $condition = null, string $type = 'INNER') static JOIN查询
 * @method $this view(mixed $join, mixed $field = null, mixed $on = null, string $type = 'INNER') static 视图查询
 * @method $this with(mixed $with) static 关联预载入
 * @method $this count(string $field) static Count统计查询
 * @method $this min(string $field) static Min统计查询
 * @method $this max(string $field) static Max统计查询
 * @method $this sum(string $field) static SUM统计查询
 * @method $this avg(string $field) static Avg统计查询
 * @method $this field(mixed $field, boolean $except = false) static 指定查询字段
 * @method $this fieldRaw(string $field, array $bind = []) static 指定查询字段
 * @method $this union(mixed $union, boolean $all = false) static UNION查询
 * @method $this limit(mixed $offset, integer $length = null) static 查询LIMIT
 * @method $this order(mixed $field, string $order = null) static 查询ORDER
 * @method $this orderRaw(string $field, array $bind = []) static 查询ORDER
 * @method $this cache(mixed $key = null , integer $expire = null) static 设置查询缓存
 * @method $this find(mixed $data = null) static 查询单个记录
 * @method $this[] select(mixed $data = null) static 查询多个记录
 * @method $this[]|Paginator paginate($listRows = null, $simple = false, $config = []) static 分页器
 */
abstract class Model extends \think\Model
{
    protected $autoWriteTimestamp;


    public function __construct($data = [])
    {
        if (empty($this->name)) {
            // 当前模型名
            $name       = str_replace('\\', '/', static::class);
            $this->name = basename($name);
            if (Container::get('config')->get('class_suffix')) {
                // $suffix     = basename(dirname($name));
                // $this->name = substr($this->name, 0, -strlen($suffix));
                $this->name = preg_replace('/Model$/', '', $this->name);
            }
        }

        parent::__construct($data);

    }

    /**
     * {@inheritDoc}
     */
    public function morphTo($morph = null, $alias = [])
    {
        $trace    = debug_backtrace(false, 2);
        $relation = Loader::parseName($trace[1]['function']);

        if (is_null($morph)) {
            $morph = $relation;
        }
        // 记录当前关联信息
        if (is_array($morph)) {
            list($morphType, $foreignKey) = $morph;
        } else {
            $morphType  = $morph . '_type';
            $foreignKey = $morph . '_id';
        }
        return new MorphTo($this, $morphType, $foreignKey, $alias, $relation);
    }
}