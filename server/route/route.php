<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\facade\Route;
use think\Request;
use ke\graphql\GraphQl;
use app\admin\services\AdminService;

Route::pattern('action', '(add|edit)');
Route::pattern('id', '\d+');
Route::pattern('key', '.+?');

Route::miss(function () {
    return response('')->code(404);
});

Route::post('admin/graphql', function (Request $request) {
    return GraphQl::build(AdminService::class)->send([], []);
});
