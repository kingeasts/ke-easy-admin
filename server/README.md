## 环境要求

php 7.4 (开发环境，7.2或许也可以)

redis

mysql 5.7

## 初始化

```
composer install
php think migrate:run
php think seed:run
```

建议安装phpstorm插件：PHP Annotations，因为它能让php注解有智能提示

## 注解路由

需要运行命令：
```
kroute --watch
# 部署时
kroute
```

## 控制器注解

* 系统默认使用登陆校验
* 用在类注释上为整个控制器生效
* 用在方法注释上为此方法生效

> 忽略登陆校验
```
@NoLogin()
```

> 权限校验
```
@UseAuth(policy="权限名")
```

> 忽略权限校验,优先级max
```
@NoAuth()
```
